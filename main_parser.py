import json
import re

import requests
from bs4 import BeautifulSoup

from Property import Property
from databasetools import insert

DADATA_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address'
DADATA_TOKEN = 'ef97fa372727012ab576c45d2bb3295443cc3444'

EGRP365_BASE_URL = 'https://egrp365.ru'
EGRP365_LIST_URL = 'https://egrp365.ru/list4.php'
EGRP365_REESTR_URL = 'https://egrp365.ru/reestr'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90' \
             'Safari/537.36'


def start_parser(address):
    dadata_response = requests.post(DADATA_URL,
                                    data=json.dumps({'query': address}),
                                    headers={'Content-Type': 'application/json', 'Accept': 'application/json',
                                             'Authorization': f'Token {DADATA_TOKEN}'})
    dadata_response_suggestions = json.loads(dadata_response.text).get('suggestions')

    if not dadata_response_suggestions:
        print('По введёному адресу ничего не найдено')
        return
    data = dadata_response_suggestions[0].get('data')
    params_dict = {'street': data.get('street'), 'house': data.get('house'), 'building': data.get('block'),
                   'mregion': data.get('region'), 'city': data.get('city'),
                   'apartment': data.get('flat'), 'link': 'page', 'fiasid': data.get('fias_id')}
    egrp_response = requests.get(EGRP365_LIST_URL, params=params_dict, headers={'user-agent': USER_AGENT})
    egrp_response = json.loads(egrp_response.text)

    if egrp_response['error'] == 0:
        html = egrp_response['data']
        soup = BeautifulSoup(html, 'html.parser')
        try:
            href = soup.find('a', text=re.compile(params_dict.get('city'))).get('href')
        except Exception:
            href = soup.find('a').get('href')

        object_info = requests.get(f'{EGRP365_BASE_URL}{href}', headers={'user-agent': USER_AGENT}).text
        object_info = BeautifulSoup(object_info, 'html.parser')
        object_info = object_info.find('div', attrs={'id': 'information_about_object'})

        try:
            map_link = object_info.find('a').get('href')
        except Exception:
            map_link = None
        params_dict['kadastr_map_link'] = map_link
        object_info = object_info.text

        try:
            kadastr_number = re.search(r'\d+:\d+:\d+:\d+', object_info)[0]
        except Exception:
            kadastr_number = None
        params_dict['kadastr_number'] = kadastr_number

        try:
            floor_index_start = object_info.find('Этаж')
            floor_index_end = object_info.find('</br>', floor_index_start)
            floor = re.search(r'\d+', object_info[floor_index_start:floor_index_end])[0]
            floor = int(floor)
        except Exception:
            floor = None
        params_dict['floor'] = floor

        try:
            square_index_start = object_info.find('Площадь')
            square_index_end = object_info.find('</br>', square_index_start)
            square = re.search(r'\d+[.,]*\d*', object_info[square_index_start:square_index_end])[0]
            square = float(square.replace(',', '.'))
        except Exception:
            square = None
        params_dict['square'] = square
        params_dict['is_address_found'] = True

    elif egrp_response['error'] == 1:
        print('АДРЕС НЕ НАЙДЕН')
        params_dict['is_address_found'] = False

    params_dict['found_address'] = data.get('unrestricted_value')
    params_dict['latitude'] = data.get('geo_lat')
    params_dict['longitude'] = data.get('geo_lon')
    params_dict['entered_address'] = address
    params_dict['found_address'] = dadata_response_suggestions[0].get('unrestricted_value')
    del params_dict['link']

    print('АДРЕС НАЙДЕН')
    print(f'Кадастровый номер: {params_dict.get("kadastr_number")}')
    insert(Property(**params_dict))


if __name__ == '__main__':
    print("Добро пожаловать в систему поиска по сайту egrp365.ru!\n"
          "Ввести адрес одной строкой           ->       (1)\n"
          "Ввести адрес отдельными параметрами  ->       (2)\n")
    n = int(input("Введите 1 или 2, чтобы выбрать режим ввода адреса: "))
    address = None
    if n == 1:
        address = input("Введите адрес: ")
    elif n == 2:
        region = input("Регион: ")
        city = input("Город: ")
        street = input("Улица: ")
        house = input("Номер дома: ")
        apartment = input('Квартира: ')
        address = f"{region}, город {city}, {street}, дом {house}, квартира {apartment}"
    if address:
        start_parser(address)
