class Property(object):
    def __init__(self=None, kadastr_number=None,
                 mregion=None,
                 city=None,
                 street=None,
                 building=None,
                 house=None,
                 apartment=None,
                 kadastr_map_link=None,
                 fiasid=None,
                 floor=None,
                 square=None,
                 latitude=None,
                 longitude=None,
                 is_address_found=None,
                 found_address=None,
                 entered_address=None,
                 link=None) -> None:
        self.kadastr_number = kadastr_number
        self.mregion = mregion
        self.city = city
        self.street = street
        self.building = building
        self.house = house
        self.apartment = apartment
        self.kadastr_map_link = kadastr_map_link
        self.fiasid = fiasid
        self.floor = floor
        self.square = square
        self.latitude = latitude
        self.longitude = longitude
        self.is_address_found = is_address_found
        self.found_address = found_address
        self.entered_address = entered_address
