import psycopg2 as psycopg2
from dbconfig import *


# Создание таблицы
'''
try:
    conn = psycopg2.connect(host=HOST, database=DATABASE, user=USER, password=PASSWORD)
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE property (
      id                        SERIAL PRIMARY KEY,
      kadastr_number            varchar(25),
      region                    varchar(255),
      city                      varchar(255),
      street                    varchar(255),
      building                  varchar(50),
      house                     varchar(50),
      apartment                 varchar(50),
      kadastr_map_link          varchar(255),
      fiasid                    varchar(255),
      floor                     integer,
      square                    float(15),
      latitude                  float(15),
      longitude                 float(15),
      found_address             varchar(255),
      entered_address           varchar(255),
      is_address_found          bool
    );""")
    conn.commit()
    cursor.close()
except Exception as error:
    print(error, 'kek loh')
finally:
    if conn is not None:
        conn.close()

'''


def insert(prop):
    sql = ''' 
    INSERT INTO property(
    kadastr_number,
    region,
    city,
    street,
    building,
    house,
    apartment,
    kadastr_map_link,
    fiasid,
    floor,
    square,
    latitude,
    longitude,
    is_address_found,
    found_address,
    entered_address) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s); 
    '''
    try:
        conn = psycopg2.connect(host=HOST, database=DATABASE, user=USER, password=PASSWORD)
        cur = conn.cursor()
        cur.execute(sql, (
            prop.kadastr_number,
            prop.mregion,
            prop.city,
            prop.street,
            prop.building,
            prop.house,
            prop.apartment,
            prop.kadastr_map_link,
            prop.fiasid,
            prop.floor,
            prop.square,
            prop.latitude,
            prop.longitude,
            prop.is_address_found,
            prop.found_address,
            prop.entered_address))
        conn.commit()
        cur.close()
    except Exception as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
