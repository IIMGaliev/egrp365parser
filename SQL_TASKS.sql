-- 1
SELECT (SELECT COUNT(*) as nf FROM property WHERE is_address_found = FALSE) as NOT_FOUND_OBJECTS_COUNT,
       (SELECT COUNT(*) FROM property WHERE is_address_found)               AS FOUND_OBJECTS_COUNT;
-- 2
SELECT COUNT(*) FROM property WHERE lower(found_address) = lower(entered_address);

-- 3
--